const cssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const commonConfig = require('./webpack.config.common');
const mergeIntoSingleFilePlugin = require('webpack-merge-and-include-globally');
const terserPlugin = require('terser-webpack-plugin');
const path = require('path');
const miniCssExtractPlugin = require('mini-css-extract-plugin');
const htmlWebpackTagsPlugin = require('html-webpack-tags-plugin');
const removePlugin = require('remove-files-webpack-plugin');
const htmlWebpackPlugin = require('html-webpack-plugin');
const copyPlugin = require('copy-webpack-plugin');
const extractTextPlugin = require('extract-text-webpack-plugin');
const { HtmlWebpackSkipAssetsPlugin } = require('html-webpack-skip-assets-plugin');
const { merge } = require('webpack-merge');

module.exports = merge(
  commonConfig,
  {
    mode: 'production',
    devtool: 'none',
    output: {
      filename: 'assets.js',
      path: path.resolve(
        __dirname,
        '../dist'
      )
    },
    module: {
      rules: [
        {
          test: /\.(sa|sc|c)ss$/,
          use: [
            miniCssExtractPlugin.loader,
            'css-loader',
            'resolve-url-loader',
            'sass-loader'
          ]
        },
        {
          test: /\.(otf|ttf|eot|woff|woff2|svg)$/,
          use: {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: '../fonts',
              esModule: false
            }
          }
        },
        {
          test: /\.css$/,
          use: extractTextPlugin.extract({
            use: [
              'css-loader',
              'sass-loader'
            ],
            fallback: 'style-loader'
          })
        }
      ]
    },
    optimization: {
      minimizer: [
        new cssMinimizerPlugin({
          minimizerOptions: {
            preset: [
              'default',
              {
                discardComments: { removeAll: true }
              }
            ]
          }
        }),
        new terserPlugin({
          terserOptions: {
            output: {
              comments: false
            }
          },
          extractComments: false
        })
      ]
    },
    plugins: [
      new copyPlugin({
        patterns: [
          {
            from: path.join(
              __dirname,
              '../img'
            ),
            to: path.resolve(
              __dirname,
              '../dist/img'
            )
          }
        ]
      }),
      new miniCssExtractPlugin({
        filename: 'css/bundle.min.css'
      }),
      new mergeIntoSingleFilePlugin({
        files: {
          'js/bundle.min.js': [
            path.resolve(
              __dirname,
              '../js/jquery.slim.js'
            ),
            path.resolve(
              __dirname,
              '../js/popper.js'
            ),
            path.resolve(
              __dirname,
              '../js/bootstrap.js'
            )
          ]
        },
        transform: {
          'js/bundle.min.js': (code) => require('uglify-js').minify(code).code
        }
      }),
      new htmlWebpackPlugin({
        filename: 'index.html',
        inject: true,
        template: path.resolve(
          __dirname,
          '../index.html'
        )
      }),
      new HtmlWebpackSkipAssetsPlugin({
        skipAssets: ['assets.js']
      }),
      new htmlWebpackTagsPlugin({
        tags: ['js/bundle.min.js'],
        append: true
      }),
      new removePlugin({
        after: {
          root: './dist',
          include: ['assets.js'],
          trash: true,
          log: false
        }
      })
    ]
  }
);
