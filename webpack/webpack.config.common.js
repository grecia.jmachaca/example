const copyWebpackPlugin = require('copy-webpack-plugin');
const path = require('path');
const removePlugin = require('remove-files-webpack-plugin');

module.exports = {
  entry: [
    './js/bootstrap.js',
    './styles/scss_bootstrap/bootstrap.scss',
    './styles/scss_font/font-awesome.scss'
  ],
  performance: {
    hints: false
  },
  output: {
    path: path.resolve(
      __dirname,
      '../dist'
    )
  },
  stats: {
    logging: 'error',
    builtAt: false,
    assets: false,
    moduleAssets: false,
    children: false,
    entrypoints: false,
    performance: false,
    timings: false,
    version: false,
    hash: false,
    modules: false
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        styles: {
          name: 'styles',
          test: /\.css$/,
          chunks: 'all',
          enforce: true
        }
      }
    }
  },
  plugins: [
    new removePlugin({
      before: {
        include: ['./dist'],
        log: false,
        logWarning: false
      },
      watch: {
        include: [
          './dist/img',
          './dist/css',
          './dist/js'
        ],
        log: false,
        logWarning: false
      }
    }),
    new copyWebpackPlugin({
      patterns: [
        {
          from: path.join(
            __dirname,
            '../styles/fonts'
          ),
          to: path.resolve(
            __dirname,
            '../dist/fonts'
          )
        }
      ]
    })
  ]
};
