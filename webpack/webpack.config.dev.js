const commonConfig = require('./webpack.config.common');
const htmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const webpack = require('webpack');
const { merge } = require('webpack-merge');
const environment = require('../environment');
const port = environment.PORT;

module.exports = merge(
  commonConfig,
  {
    mode: 'development',
    devtool: 'eval',
    watch: true,
    output: {
      filename: '[name].[hash].js',
      path: path.resolve(
        __dirname,
        '../dist'
      )
    },
    devServer: {
      host: 'localhost',
      port,
      hot: true,
      watchContentBase: true,
      writeToDisk: true,
      compress: true,
      contentBase: './dist',
      onListening() {

        console.info(`> Listening on ${port}`);

      }
    },
    plugins: [
      new webpack.HotModuleReplacementPlugin(),
      new htmlWebpackPlugin({
        filename: 'index.html',
        inject: true,
        template: path.resolve(
          __dirname,
          '../index.html'
        )
      })
    ],
    module: {
      rules: [
        {
          test: /\.(sa|sc|c)ss$/,
          use: [
            'style-loader',
            'css-loader',
            'resolve-url-loader',
            'sass-loader'
          ]
        },
        {
          test: /\.(otf|ttf|eot|woff|woff2|svg)$/,
          use: {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: '/fonts',
              esModule: false
            }
          }
        },
        {
          test: require.resolve('../js/jquery.slim'),
          loader: 'expose-loader',
          options: {
            exposes: [
              '$',
              'jQuery'
            ]
          }

        }
      ]
    },
    externals: {
      jquery: 'jQuery'
    }
  }
);
