## Getting Started

To modify styles and javascript files:

```bash
npm run dev
```

To generate dist files (css, js and images):

```bash
npm run build
```
To start the static file server:

```bash
npm run start
```

## Register all runners

- Shell Runner: 
```
sudo gitlab-runner register --non-interactive --url="https://w0gitcicd01.grupobisa.net/" --registration-token="oVg9k5_2X391MzWYCYfS" --executor="shell" --description="shell-runner" --tag-list="shell-runner" --run-untagged="false" --locked="true" --access-level="ref_protected"
```

- Docker Runner: 
```
sudo gitlab-runner register --non-interactive --url="https://w0gitcicd01.grupobisa.net/" --registration-token="oVg9k5_2X391MzWYCYfS" --executor="docker" --description="docker-runner" --tag-list="docker-runner" --run-untagged="false" --locked="true" --access-level="ref_protected" --docker-image="docker:19.03.12" --docker-privileged="true"
```
