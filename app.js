const environment = require('./environment');
const originsAllowed = require('./commons/tools');
const express = require('express');
const port = environment.PORT;
const cors = require('cors');
const compression = require('compression');
const app = express();

app.use(cors({ origin: originsAllowed }));

app.use(compression({
  level: 9,
  memLevel: 9
}));

app.use(express.static(`${__dirname}/dist`));

app.listen(
  port,
  () => {

    console.info(`> Listening on: ${port}`);
    console.info(`> Listening on: ${port}`);

  }

);
