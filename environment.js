module.exports = {
  TEST: {
    TIMEOUT: 50000
  },
  ORIGINS: {
    ALLOWED: [
      'http://localhost:3000',
      'http://3.22.96.209:8888'
    ]
  },
  PORT: 3005,
  PROPERTIES: {
    NAME: 'my-application',
    ENDPOINT: 'http://3.22.206.234:8888',
    PROFILES: ['dev'],
    HEADERS: { 'X-Config-Token': 's.e2lbX0Gz1rm2fZHSSAVGjgvA' }
  },
  STATIC: ''
};
