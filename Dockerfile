FROM node:12.18.2-alpine

WORKDIR /usr/src/app

COPY . .

EXPOSE 3005

CMD ["npm", "run", "start"]
