const { NUMBERS } = require('./commons/enums');

let suite = 'default';

process.argv.forEach((value) => {

  if (value.startsWith('--suite')) {

    suite = value.split('=')[NUMBERS.ONE];

  }

});

module.exports = {
  verbose: true,
  testEnvironment: 'node',
  setupFiles: ['<rootDir>/jest.setup.js'],
  testPathIgnorePatterns: [
    '<rootDir>/dist/',
    '<rootDir>/fonts/',
    '<rootDir>/js/',
    '<rootDir>/styles/',
    '<rootDir>/node_modules/'
  ],
  reporters: [
    'default',
    [
      'jest-html-reporters',
      {
        publicPath: './test',
        filename: `${suite}-test-report.html`,
        expand: false
      }
    ]
  ]
};
