const environment = require('../environment');
const { NUMBERS, ERRORS } = require('./enums');

const allowedOrigins = environment.ORIGINS.ALLOWED;
const indexOrigin = NUMBERS.MINUS.ONE;
const error = ERRORS.CORS;

const originsAllowed = function (origin, callback) {

  if (!origin) {

    return callback(
      null,
      true
    );

  }

  if (allowedOrigins.indexOf(origin) === indexOrigin) {

    return callback(
      new Error(error),
      false
    );

  }

  return callback(
    null,
    true
  );

};

const checkParameter = function (parameter, type) {

  let isValid = false;

  if (
    parameter !== null &&
    typeof parameter !== 'undefined'
  ) {

    isValid = true;

  }

  if (
    type !== null &&
    typeof type !== 'undefined' &&
    typeof type === 'string'
  ) {

    if (typeof parameter === type) {

      isValid = true;

    } else {

      isValid = false;

    }

  }

  return isValid;

};

module.exports = {
  checkParameter,
  originsAllowed
};
