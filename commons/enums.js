const ERRORS = {
  CORS: 'The CORS policy for this site does not allow access from the specified origin.',
  PROPERTIES: 'Error on getting properties.'
};

const NUMBERS = {
  MINUS: {
    ONE: -1
  },
  ONE: 1
};

const TYPES = {
  STRING: 'string'
};

module.exports = {
  ERRORS,
  NUMBERS,
  TYPES
};
