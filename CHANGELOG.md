

<!--- next entry here -->

## 1.8.17
2020-09-23

### Fixes

- **dockerfile:** created push tag docker (d6a7878c8f1673f5a8b451da1de5d7e0ffb43cca)

## 1.8.16
2020-09-23

### Fixes

- **dockerfile:** adding tag registry (97eaabdbb8e3a03296520b59f4f87a6f56bb215d)

## 1.8.15
2020-09-23

### Fixes

- **dockerfile:** adding tag registry (e755bf6b46fbea9b7b797e9238915038671c83d7)

## 1.8.14
2020-09-23

### Fixes

- **dockerfile:** adding docker host (a6ba2eb3cf2b11bb8f23784f8ad8a4fa361521c5)

## 1.8.13
2020-09-23

### Fixes

- **dockerfile:** created (a8e948dcfaa61777d16e16c686162ebcb7182fd9)
- **dockerfile:** adding registry (2089d683cee1010b5909ac68cbc2ec9011534a6e)

## 1.8.12
2020-09-23

### Fixes

- **dockerfile:** created (405878078fcf863aaabead4c785e11ad4ec041a3)
- **dockerfile:** created (74c56c30246c219474b81895df0de7a9e87184ec)

- 1.8.10
- fix changelog
- 1.8.9
- changelog
- 1.8.8
- cat
- 1.8.7
- changelog
- 1.8.6
- changelog
- 1.8.5
- verifying changelog
- 1.8.4
- adding source
- 1.8.3
- add variable
- 1.8.2
- 1.8.1
- adding changelog
- Add CHANGELOG
- 1.8.0
- adding changelog
- 1.7.0
- adding tag to only
- 1.6.0
- 1.5.0
- 1.4.0
- removing if rule
- 1.3.4
- fix version and adding assets
- 1.3.3
- adding assets
- 1.3.2
- change release
- 1.3.1
- changing version
- change version
- adding descripcion
- change version
- fix extract version
- 1.3.0
- fix format
- 1.2.0
- new release cli
- 1.1.11
- setting version release cli
- 1.1.10
- fix img
- 1.1.9
- version releasecli
- 1.1.8
- rule fixed
- rule for release
- test variable
- verifying variable
- remove service
- quit vi
- version in dev
- see file
- adding version to file
- 1.1.7
- adding tag only
- 1.1.6
- 1.1.5
- fix
- 1.1.4
- adding release
- 1.1.4-0
- 1.1.3
- set version
- removing version tag
- fix version
- see var
- export
- format
- format
- before script
- before script
- tag
- see tag
- adding docker image
- global variable
- new stage
- watching file
- saving in a file
- try global variable
- package
- package
- package json
- tag
- tag fix
- tag fix
- try
- export
- getting version
- getting version
- see version
- fix version
- verification of version
- generated version
- see if version is generated
- npm run version fix
- 1.1.2
- adding version
- version
- version
- version
- test version
- version
- fix
- 1.1.1
- 1.1.0
- fix
- see version
- adding version
- install jq
- 1.0.1
- release
- ci server version
- see variables
- see vars
- see changes
- login
- ordering
- login
- fix vars
- fix vars
- see vars
- fix tag
- registry image
- adding download
- adding build part
- no avj
- refactor
- refactor
- running example
- try tag
- change v docker
- adding img tag
- change docker version
- node latest
- without pull
- change node image
- pull tag
- tag fix
- build step
- copy static file server
- fix tag
- copy static file server
- dockerfile
- format
- format
- quit point
- adding pull to tag
- fix tag
- registry
- fix registry
- registry fixed
- gitlab registry
- ci skip
- ci skip
- fix url
- other private token
- private token
- via api
- other private token
- private token
- curl
- gitlab token
- ssh
- ssh key
- order ssh
- ssh
- ssh
- with deploy token
- with ci token
- through gitlab ci token
- token
- test
- access token user login
- access token
- see personal access token
- variable personal access token
- adding origin
- without http
- format
- with job token
- external variable v2
- variable external
- with personal token
- order tag
- tag
- release tag
- without url
- ignore by git
- test git status
- test with token
- test2
- test
- test
- test
- update
- adding read me to test
- update token
- update ci/cd
- update ci/cd
- update user
- UPDATE
- update
- Update .gitlab-ci.yml
- Update .gitlab-ci.yml
- Update .gitlab-ci.yml
- Update .gitlab-ci.yml
- Update .gitlab-ci.yml
- Update .gitlab-ci.yml
- Update .gitlab-ci.yml
- Update .gitlab-ci.yml
- Update .gitlab-ci.yml
- package log
- package.json
- Update .gitlab-ci.yml
- Update .gitlab-ci.yml
- Update .gitlab-ci.yml
- Update .gitlab-ci.yml
